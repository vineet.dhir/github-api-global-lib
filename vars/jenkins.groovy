def install(application) {
    echo "Hi, Your function is started"
    sh 'sudo apt-get update'
    sh 'sudo apt install ${application}'
    echo "Hi, Your function is closed"
}

def copy(loc1,loc2) {
    echo "Hi, Your function is started"
    sh 'cp ${loc1} ${loc2}'
    echo "Hi, Your function is closed"
}

def service(app,work) {
    echo "Hi, Your function is started"
    sh 'sudo systemctl cp ${work} ${app}'
    echo "Hi, Your function is closed"
}

def download(url) {
    echo "Hi, Your function is started"
    sh 'wget -P /opt/apps ${url}'
    echo "Hi, Your function is closed"
}

def directory(path) {
    echo "Hi, Your function is started"
    sh 'sudo mkdir ${path}'
    echo "Hi, Your function is closed"
}

def extract(file_path,destination_path) {
    echo "Hi, Your function is started"
    sh 'tar -xvzf ${file_path} -C ${destination_path}'
    echo "Hi, Your function is closed"
}















