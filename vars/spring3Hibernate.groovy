def clone(Map config = [:]) {
    echo "Hi, Your function is started"
    git branch: "${config.branch}", credentialsId: 'Gitlab-SSH', url: "${config.giturl}"
    echo "Hi, Your function is closed"
}

def compile(Map config = [:]) {
    echo "Hi, Your function is started"
    sh 'mvn clean'
    sh 'mvn compile'
    echo "Hi, Your function is closed"
}

def pmd(Map config = [:]) {
    echo "PMD Analysis"
    sh 'mvn pmd:pmd'
}

def quality(Map config = [:]) {
    echo "Code Quality Analysis"
    sh 'mvn checkstyle:checkstyle'
}

def coverage(Map config = [:]) {
    echo "Code Coverage Analysis"
    sh 'mvn cobertura:cobertura'
}








