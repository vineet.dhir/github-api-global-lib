def install(application) {
    stage ('install') {
        echo "Hi, Your function is started"
        sh 'sudo apt-get update'
        sh 'sudo apt install ${application}'
        echo "Hi, Your function is closed"
    }

def copy(loc1,loc2) {
    stage ('copy') {
        echo "Hi, Your function is started"
        sh 'cp ${loc1} ${loc2}'
        echo "Hi, Your function is closed"
    }
    
def service(app,work) {
    stage ('service') {
        echo "Hi, Your function is started"
        sh 'sudo systemctl cp ${work} ${app}'
        echo "Hi, Your function is closed" 
    }
    
def download(url) {
    stage ('download') {
        echo "Hi, Your function is started"
        sh 'wget -P /opt/apps ${url}'
        echo "Hi, Your function is closed"
    }

def directory(path) {
    stage ('directory') {
        echo "Hi, Your function is started"
        sh 'sudo mkdir ${path}'
        echo "Hi, Your function is closed"
    }
    
def extract(file_path,destination_path) {
    stage ('extract') {
        echo "Hi, Your function is started"
        sh 'tar -xvzf ${file_path} -C ${destination_path}'
        echo "Hi, Your function is closed"
    }

def adduser(user_name) {
    stage ('adduser') {
        echo "Hi, Your function is started"
        sh 'sudo useradd -m ${user_name} -s /bin/bash'
        echo "Hi, Your function is closed"
    }

def addgroup(group_name) {
    stage ('addgroup') {
        echo "Hi, Your function is started"
        sh 'sudo groupadd ${group_name}'
        echo "Hi, Your function is closed"
    }

def deleteuser(user_name) {
    stage ('deleteuser') {
        echo "Hi, Your function is started"
        sh 'sudo userdel -r ${username}'
        echo "Hi, Your function is closed"
    }

def addusertogroup(user_name) {
    stage ('addusertogroup') {
        echo "Hi, Your function is started"
        sh 'sudo usermod -a -G ${group_name} ${user_name}'
        echo "Hi, Your function is closed"
    }
